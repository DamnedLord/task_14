#include <iostream>
#include <string>

int main()
{
    const std::string SampleString = "The quick brown fox jumps over the lazy dog";

    // ленивый вариант
    std::cout << SampleString << std::endl
        << SampleString.length() /*.size() привычнее*/ << std::endl
        << *SampleString.begin() << std::endl
        << *SampleString.rbegin() << std::endl;

    // попроще ;)
    std::cout << std::endl << SampleString << std::endl
        << SampleString.length() << std::endl
        << SampleString[0] << std::endl
        << SampleString[SampleString.length() - 1] << std::endl;
}